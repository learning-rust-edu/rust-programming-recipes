extern crate p1_filework;
use failure::Error;
use p1_filework::*;

fn main() -> Result<(), Error> {
    let fname = "test_data/transactions.json";
    // .expect() do exactly what unwrap() do but allow to have custom message
    let trans = get_transactions(fname).expect("Could not load transactions");
    for t in trans {
        println!("{:?}", t);
    }

    let t = get_first_transaction_for(fname, "Matt");
    match t {
        Ok(v) => println!("Found transaction: {:?}", v),
        Err(e) => println!("Error {}, Backtrace = {}", e, e.backtrace()),
    }

    Ok(())
}
